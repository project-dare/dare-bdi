image: alpine:latest

stages:
  - build
  - package
  - review
  - release

variables:
  # AUTO_DEVOPS_DOMAIN: domain.example.com

  DOCKER_DRIVER: overlay2
  KUBERNETES_VERSION: 1.8.6
  HELM_VERSION: 2.10.0

build_container:
  stage: build
  image: docker:stable-git
  services:
  - docker:stable-dind
  variables:
    IMAGE_TAG_REF: $CI_REGISTRY_IMAGE/$CONTAINER_NAME:$RELEASE_REF
  script:
  - setup_docker
  - registry_login
  - (cd containers/$CONTAINER_NAME;
      docker build --pull -t $IMAGE_TAG_REF --build-arg RELEASE="$RELEASE_REF" --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') .)
  - docker push $IMAGE_TAG_REF
  only:
    refs:
    - pipelines
    - master
    variables:
    - $CONTAINER_NAME
    - $RELEASE_REF
  except:
    variables:
    - $RELEASE_REF == "master"

build_container_latest:
  stage: build
  image: docker:latest
  services:
  - docker:dind
  variables:
    IMAGE_TAG_REF: $CI_REGISTRY_IMAGE/$CONTAINER_NAME
  script:
  - setup_docker
  - registry_login
  - (cd containers/$CONTAINER_NAME;
        docker pull $IMAGE_TAG_REF || true;
        docker build -t $IMAGE_TAG_REF --cache-from $IMAGE_TAG_REF --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') .)
  - docker push $IMAGE_TAG_REF
  only:
    refs:
    - pipelines
    - master
    variables:
    - $CONTAINER_NAME

build_containers:
  stage: build
  image: docker:latest
  services:
  - docker:dind
  script:
  - setup_docker
  - registry_login
  - (cd containers; for c in `ls -d */ | cut -f1 -d '/'`; do
      (cd $c;
        docker pull $CI_REGISTRY_IMAGE/$c || true;
        docker build -t "$CI_REGISTRY_IMAGE/$c" --cache-from "$CI_REGISTRY_IMAGE/$c" --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') .);
      docker push "$CI_REGISTRY_IMAGE/$c";
    done)
  only:
  - master

build_containers_branches:
  stage: build
  image: docker:latest
  when: manual
  services:
  - docker:dind
  variables:
    IMAGE_TAG_REF: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG
  script:
  - setup_docker
  - registry_login
  - (cd containers; for c in `ls -d */ | cut -f1 -d '/'`; do
      (cd $c;
        docker pull $CI_REGISTRY_IMAGE/$c || true;
        docker build --pull -t "$IMAGE_TAG_REF/$c" --cache-from "$CI_REGISTRY_IMAGE/$c" --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') .);
      docker push "$IMAGE_TAG_REF/$c";
    done)
  except:
  - master

lint_charts:
  image: registry.gitlab.com/charts/alpine-helm
  stage: package
  when: always
  script:
  - helm init --client-only
  - helm dep up
  - helm lint
  - mkdir -p build
  - helm package -d build .
  artifacts:
    expire_in: 3d
    paths:
    - build
  except:
  - tags

# review:
#  stage: review
#  environment:
#    name: review/$CI_COMMIT_REF_NAME
#    url: https://$CI_ENVIRONMENT_SLUG.$AUTO_DEVOPS_DOMAIN
#    on_stop: stop_review
#  only:
#    refs:
#    - branches
#    kubernetes: active
#  except:
#  - master

# stop_review:
#  stage: review
#  environment:
#    name: review/$CI_COMMIT_REF_NAME
#    action: stop
#  when: manual
#  allow_failure: true
#  only:
#    refs:
#    - branches
#    kubernetes: active
#  except:
#  - master

release_charts:
  stage: release
  script:
  - curl --request POST
    --form "token=$CI_JOB_TOKEN"
    --form ref=master
    --form "variables[CHART_NAME]=$CI_PROJECT_NAME"
    --form "variables[RELEASE_REF]=$CI_COMMIT_REF_NAME"
    https://gitlab.com/api/v4/projects/10917887/trigger/pipeline
  only:
  - tags@project-dare/dare-bdi

.auto_devops: &auto_devops |

  function registry_login() {
    if [[ -n "$CI_REGISTRY_USER" ]]; then
      echo "Logging to GitLab Container Registry with CI credentials..."
      docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
      echo ""
    fi
  }

  function setup_docker() {
    if ! docker info &>/dev/null; then
      if [ -z "$DOCKER_HOST" -a "$KUBERNETES_PORT" ]; then
        export DOCKER_HOST='tcp://localhost:2375'
      fi
    fi
  }

  function install_dependencies() {
    apk add -U openssl curl tar gzip bash ca-certificates git
    curl -L -o /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub
    curl -L -O https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.28-r0/glibc-2.28-r0.apk
    apk add glibc-2.28-r0.apk
    rm glibc-2.28-r0.apk

    curl "https://kubernetes-helm.storage.googleapis.com/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar zx
    mv linux-amd64/helm /usr/bin/
    helm version --client

    curl -L -o /usr/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/v${KUBERNETES_VERSION}/bin/linux/amd64/kubectl"
    chmod +x /usr/bin/kubectl
    kubectl version --client
  }

  function ensure_namespace() {
    kubectl describe namespace "$KUBE_NAMESPACE" || kubectl create namespace "$KUBE_NAMESPACE"
  }

  function check_kube_domain() {
    if [ -z ${AUTO_DEVOPS_DOMAIN+x} ]; then
      echo "In order to deploy or use Review Apps, AUTO_DEVOPS_DOMAIN variable must be set"
      echo "You can do it in Auto DevOps project settings or defining a variable at group or project level"
      echo "You can also manually add it in .gitlab-ci.yml"
      false
    else
      true
    fi
  }

  function install_tiller() {
    echo "Checking Tiller..."
    helm init --upgrade
    kubectl rollout status -n "$TILLER_NAMESPACE" -w "deployment/tiller-deploy"
    if ! helm version --debug; then
      echo "Failed to init Tiller."
      return 1
    fi
    echo ""
  }

  function delete() {
    track="${1-stable}"
    name="$CI_ENVIRONMENT_SLUG"

    if [[ "$track" != "stable" ]]; then
      name="$name-$track"
    fi

    if [[ -n "$(helm ls -q "^$name$")" ]]; then
      helm delete --purge "$name"
    fi
  }

before_script:
  - date
  - *auto_devops

after_script:
  - date
