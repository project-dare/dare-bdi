#! /bin/sh


pwd=dba
graph="http://localhost:8890/DAV"
tmpdir=/data/toLoad
loadfile='load.n3'
tmpfile="$tmpdir/$loadfile"
sprov_host=localhost
sprov_port=80

if [ "$DBA_PASSWORD" ]; then pwd="$DBA_PASSWORD" ; fi
if [ "$DEFAULT_GRAPH" ]; then graph="$DEFAULT_GRAPH" ; fi

mkdir -p $tmpdir

curl "http://$sprov_host:$sprov_port/j2ep-1.0/prov/workflowexecutions/$1/export?format=rdf" > $tmpfile

isql-v -U dba -P "$pwd" exec="ld_dir('$tmpdir', '$loadfile' , '$graph');"

isql-v -U dba -P "$pwd" exec="rdf_loader_run();"

isql-v -U dba -P "$pwd" exec="checkpoint;"
