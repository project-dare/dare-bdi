#! /bin/sh

export RAAS_LOGGING="True"

CONN=$SPROV_DB_HOST:$SPROV_DB_PORT/$SPROV_DB

export RAAS_REPO="mongodb://$CONN"

mongo $CONN ../../resources/ensure_indexes.js
mongo $CONN ../../resources/lineage_map_reduce.js

gunicorn -w 9 -b 0.0.0.0:8082 "flask_raas:bootstrap_app()" \
        --log-level debug \
        --backlog 0 \
        --timeout 120 
